<?php
declare(strict_types=1);

namespace Beside\Elitbuzztechnologies\Api;

/**
 * Class ElitbuzzConnectorInterface
 *
 * @package Beside\Elitbuzztechnologies\Api
 */
interface ElitbuzzConnectorInterface
{
    /** @var string XML path to Elitbuzz SMS API URL config */
    public const XML_PATH_API_URL = 'beside_sms/elitbuzz/api_url';

    /** @var string XML path to Elitbuzz SMS API key config */
    public const XML_PATH_API_KEY = 'beside_sms/elitbuzz/api_key';

    /** @var string XML path to Elitbuzz SMS SenderID config */
    public const XML_PATH_SENDER_ID = 'beside_sms/elitbuzz/sender_id';

    /**
     * Send SMS, return request and response data as array
     *
     * @param string $number
     * @param string $message
     *
     * @return array
     */
    public function sendSms(string $number, string $message): array;
}
