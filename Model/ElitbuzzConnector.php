<?php
declare(strict_types=1);

namespace Beside\Elitbuzztechnologies\Model;

use Beside\Elitbuzztechnologies\Api\ElitbuzzConnectorInterface;
use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\HTTP\Client\CurlFactory;

/**
 * Class ElitbuzzConnector
 *
 * @package Beside\Elitbuzztechnologies\Model
 */
class ElitbuzzConnector implements ElitbuzzConnectorInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var CurlFactory
     */
    private CurlFactory $curlFactory;

    /**
     * ElitbuzzConnector constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param CurlFactory $curlFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CurlFactory $curlFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curlFactory = $curlFactory;
    }

    /**
     * Get URL key from configs
     *
     * @return string
     */
    private function getApiUrl(): string
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_API_URL
        );

        return (string) $result;
    }

    /**
     * Get API key from configs
     *
     * @return string
     */
    private function getApiKey(): string
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_API_KEY
        );

        return (string) $result;
    }

    /**
     * Get SenderID from configs
     *
     * @return string
     */
    private function getSenderId(): string
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_SENDER_ID
        );

        return (string) $result;
    }

    /**
     * Send SMS, return request and response data as array
     *
     * @param string $number
     * @param string $message
     *
     * @return array
     */
    public function sendSms(string $number, string $message): array
    {
        $errorMessage = null;
        $body = null;
        $status = null;

        try {
            /** @var Curl $curl */
            $curl = $this->curlFactory->create() ;
            $headers['Content-Type'] = 'application/json';
            $url = $this->getApiUrl();
            $params = $this->getParams($number, $message);
            $curl->post($url, $params);
            $curl->setHeaders($headers);
            $body = $curl->getBody();
            $status = $curl->getStatus();
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            if (!$errorMessage) {
                $errorMessage = __('An error has occurred while sending Elitbuzz SMS request');
            }
        }
        $result = [
            'url' => $url,
            'request' => $message,
            'response' => $body,
            'status' => $status,
            'errors' => $errorMessage
        ];

        return $result;
    }

    /**
     * Get query params
     *
     * @param string $number
     * @param string $message
     *
     * @return array
     */
    private function getParams(string $number, string $message): array
    {
        $params = [
            'type' => 'text',
            'api_key' => $this->getApiKey(),
            'contacts' => $number,
            'senderid' => $this->getSenderId(),
            'msg' => $message
        ];

        return $params;
    }
}
